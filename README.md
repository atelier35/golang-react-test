# QuoteMachine Technical Test #

This test has two parts: one back-end in Go and one front-end in Javascript/React.
Both part are code snippet that contain the logic of an online order with items.
The goal is to add a discount feature on order line to support percentage and fixed amount discount.

### Go ###
* The code snippet generate an order with 3 lines and we want to apply a discount on the total of the first 2 lines. 
* Modify the code snippet to have the first line with a $300 discount and the second line with a 15% discount.
* The result should show: 
```
Oder ID: ORD-0001
------------------------------

1 x [$2959.95] Red Velvet Sofa  =  $2659.95 [Reg: $2959.95 - $300]
2 x [$649.99] Yellow Harmed Chair  =  $1104.98 [Reg: $1299.98 - $195 (15%)]
5 x [$29.99] Blue Squared Cushion  =  $149.95

------------------------------
TOTAL: $3914.88 [Reg: $4409.88 - $495]
```
* [Code snippet](https://goplay.tools/snippet/rfMLJwDgFxa)
* Once your done, click on the "Share" button at the top of the screen to generate and new link that you will send by email


### Javascript/React ###
* Similarly to the back-end, modify the code snippet to display the discount amount and the regular price on the first two lines and the total.
* Before you start coding, create a codepen.io account if you don't have one and click on the "Fork" button on the right bottom of the screen.
* [Code snippet](https://codepen.io/quotemachine/pen/ExvRLOj)
* Once your done, click on "Save" button at the top of the screen and copy the url that you will send by email